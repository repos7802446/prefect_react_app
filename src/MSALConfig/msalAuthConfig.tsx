import { Configuration, LogLevel } from "@azure/msal-browser";
import { environment } from "../environment/environment";

export const msalAuthConfig: Configuration = {
    auth: {
        clientId: `${environment.clientId}`,
        authority: `${environment.microsoftUrl}`,
        redirectUri: `${environment.redirectUri}`,
        postLogoutRedirectUri: '/',
        navigateToLoginRequestUrl: false
    },
    cache: {
        cacheLocation: "sessionStorage",
        storeAuthStateInCookie: false
    },
    system: {
        loggerOptions: {
            loggerCallback: (level: any, message: any, containsPii: any) => {
                if (containsPii) {
                    return;
                }
                switch (level) {
                    case LogLevel.Error:
                        console.error(message);
                        return;

                    case LogLevel.Info:
                        console.info(message);
                        return;

                    case LogLevel.Verbose:
                        console.debug(message);
                        return;

                    case LogLevel.Warning:
                        console.warn(message);
                        return;

                    default:
                        return;
                }
            },
        },
        allowNativeBroker: false,
    }
};

export const LoginRequest = {
    scopes: ["user.read"]
};

/** Work on this to achieve silent SSO between application by providing login hint 

export const SilentRequest = {
    scopes: ["openid", "profile", "email"],
    loginHint: `${environment.redirectPrefectUri}`
};

**/