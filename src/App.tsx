import React from 'react';
import logo from './logo.svg';
import './App.css';
import { LandingPageComponent } from './components/LandingPageComponent';

function App() {
  return (
    <div className="App">
      <LandingPageComponent />
    </div>
  );
}

export default App;
