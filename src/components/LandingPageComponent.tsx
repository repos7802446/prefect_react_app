import { AccountInfo, IPublicClientApplication, InteractionStatus, InteractionType } from "@azure/msal-browser";
import { AuthenticatedTemplate, MsalAuthenticationTemplate, UnauthenticatedTemplate, useIsAuthenticated, useMsal } from "@azure/msal-react";
import { FunctionComponent } from "react";
import { Loading } from "./utils/Loading";
import { Link, useNavigate } from "react-router-dom";
import { environment } from "../environment/environment";

export const LandingPageComponent: FunctionComponent = () => {

    const { inProgress, instance, accounts } = useMsal();
    const isAuthenticated = useIsAuthenticated();

    function signInClickHandler(instance: any) {
        instance.loginRedirect();
    }

    // SignInButton Component returns a button that invokes a popup login when clicked
    function SignInButton() {
        // useMsal hook will return the PublicClientApplication instance you provided to MsalProvider
        const { instance } = useMsal();

        return (
            <div className="vh-100 vw-100 bg-dark">
                <p className="text-white">This will only render if a user is not signed-in.</p>
                <button className="btn btn-primary main-color text-white" onClick={() => signInClickHandler(instance)}>Sign In</button>
            </div>
        );
    }

    function WelcomeUser() {
        const { accounts } = useMsal();
        const username = accounts[0].username;

        return (
            <div>
                <p>Welcome, {username}</p>
            </div>
        );
    }

    // function logout() {
    //     const findAccountByHomeAccountId = (
    //         homeAccountId: string,
    //     ): AccountInfo | undefined => {
    //         return accounts.find(
    //             (account) => account.homeAccountId === homeAccountId,
    //         );
    //     };

    //     const logOut = () => {
    //         const homeAccountId = accounts[0].homeAccountId;
    //         const accountToSignOut = findAccountByHomeAccountId(homeAccountId);
    //         if (accountToSignOut) {
    //             signOutClickHandler(instance, accountToSignOut);
    //         } else {
    //             console.error("Account not found for the given homeAccountId");
    //         }
    //     };
    // }
    function signOutClickHandler(instance: any) {
        const homeAccountId = accounts[0].homeAccountId;
        const logoutRequest = {
            account: instance.getAccountByHomeId(homeAccountId),
            mainWindowRedirectUri: `${environment.redirectUri}`,
            postLogoutRedirectUri: `${environment.microsoftUrl}`
        }
        instance.logoutPopup(logoutRequest);
    }

    // function signOutClickHandler(instance: IPublicClientApplication, accountToSignOut: AccountInfo) {
    //     const logoutRequest = {
    //         account: accountToSignOut,
    //         postLogoutRedirectUri: `${environment.microsoftUrl}`,
    //     };
    //     instance.logoutRedirect(logoutRequest);
    // }
    // SignOutButton Component returns a button that invokes a popup logout when clicked
    function SignOutButton() {
        // useMsal hook will return the PublicClientApplication instance you provided to MsalProvider
        const { instance } = useMsal();

        return (
            <div className="m-2">
                <button className="btn btn-primary" onClick={() => signOutClickHandler(instance)}>Sign Out</button>
            </div>

        );
    };

    if (!isAuthenticated && inProgress != InteractionStatus.None) {
        return <Loading />;
    }
    return (
        // <MsalAuthenticationTemplate interactionType={InteractionType.Redirect}>
        //     <div className="d-flex m-2 col-12">
        //         <div className="col-md-6">
        //             <Link type="button" className="btn btn-primary" to={`${environment.redirectPrefectUri}`}>Go To Prefect</Link>
        //         </div>
        //         <div className="col-md-6">
        //             <button type="button" className="btn btn-danger pull-right text-white align-items-end" onClick={logout}>Log Out</button>
        //         </div>
        //     </div>
        // </MsalAuthenticationTemplate>
        <>
            <AuthenticatedTemplate>
                <WelcomeUser />
                {/* <p>This will only render if a user is signed-in.</p> */}
                <SignOutButton />
            </AuthenticatedTemplate>
            <UnauthenticatedTemplate>
                <SignInButton />
            </UnauthenticatedTemplate>

        </>
    );
};


