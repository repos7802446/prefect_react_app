import React, { useEffect } from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { AuthenticationResult, EventMessage, EventType, PublicClientApplication } from '@azure/msal-browser';
import { msalAuthConfig } from './MSALConfig/msalAuthConfig';
import { BrowserRouter, useNavigate } from 'react-router-dom';
import { environment } from './environment/environment';
import { MsalProvider } from '@azure/msal-react';

const msalInstance = new PublicClientApplication(msalAuthConfig);

/** get initialise msalInstance */
msalInstance.initialize();

const activeAccount = msalInstance.getActiveAccount();

/** Account Selection */
if (!activeAccount) {
  const accounts = msalInstance.getAllAccounts();
  if (accounts.length > 0) {
    msalInstance.setActiveAccount(accounts[0]);
  }
}

/** Set the account active */
msalInstance.addEventCallback((event: EventMessage) => {
  if (event.eventType === EventType.LOGIN_SUCCESS && event.payload) {
    const authenticationResult = event.payload as AuthenticationResult;
    const account = authenticationResult.account;
    msalInstance.setActiveAccount(account);
  }
});

/** Enable account storage event */
msalInstance.enableAccountStorageEvents();

function AppComponent() {

  const navigate = useNavigate();

  useEffect(() => {
    msalInstance.handleRedirectPromise().then((response) => {
      if (response && response.account) {
        /** If user is authenticated, proceed with application */
        navigate(`/LandingPageComponent`, { replace: true });
      }
    });

    /** Check if user is already signed in or not */
    const account = msalInstance.getActiveAccount();
    if (account) {
      /** User exists, proceed with application */
      navigate(`/LandingPageComponent`, { replace: true });
    } else {
      /** User doesn't exists, initiate to login process */
      msalInstance.initialize();
    }

  }, []);

  return <App />
}

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <BrowserRouter>
    <MsalProvider instance={msalInstance}>
      <AppComponent />
    </MsalProvider>
  </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
